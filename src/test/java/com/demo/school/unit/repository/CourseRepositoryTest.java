package com.demo.school.unit.repository;

import com.demo.school.entity.Course;
import com.demo.school.entity.Student;
import com.demo.school.entity.StudentCourse;
import com.demo.school.repository.CourseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CourseRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void testFindById() {

        Course course = new Course();
        course.setName("test course");
        course = entityManager.persist(course);
        entityManager.flush();

        Optional<Course> mayBeCourse = courseRepository.findById(course.getId());

        assertThat(mayBeCourse.get().getName().equals(course.getName()));
    }

    @Test
    public void testRegister() {

        Course course = new Course();
        course.setName("test course");
        course = entityManager.persist(course);
        entityManager.flush();

        Student s = new Student();
        s.setName("test student");
        s = entityManager.persist(s);
        entityManager.flush();

        StudentCourse sc = new StudentCourse();
        sc.setCourse(course);
        sc.setStudent(s);

        sc = entityManager.persist(sc);

        assertThat(sc.getCourse().getId().equals(course.getId()));
        assertThat(sc.getStudent().getId().equals(s.getId()));
    }

}
