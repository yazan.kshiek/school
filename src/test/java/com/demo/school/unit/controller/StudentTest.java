package com.demo.school.unit.controller;


import com.demo.school.config.ConfigService;
import com.demo.school.controller.StudentController;
import com.demo.school.controller.StudentCourseController;
import com.demo.school.entity.Config;
import com.demo.school.entity.Course;
import com.demo.school.entity.Student;
import com.demo.school.exception.RestApiResponse;
import com.demo.school.repository.CourseRepository;
import com.demo.school.repository.StudentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class StudentTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    StudentCourseController studentCourseController;

    @MockBean
    StudentRepository studentRepository;

    @MockBean
    CourseRepository courseRepository;

    @Test
    public void register()
            throws Exception {


        ObjectMapper objectMapper = new ObjectMapper();

        Student s = new Student("Student 1");

        mvc.perform(post("/students")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(s)))
                    .andExpect(status().isCreated())
                    .andReturn();


            Course c = new Course("Course 1");

             mvc.perform(post("/courses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(c)))
                    .andExpect(status().isCreated())
                    .andReturn();
    }
}
