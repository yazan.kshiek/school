package com.demo.school.repository;


import com.demo.school.entity.Student;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends DbRepository<Student> {
}