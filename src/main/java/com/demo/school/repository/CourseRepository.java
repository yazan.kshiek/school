package com.demo.school.repository;

import com.demo.school.entity.Course;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository  extends DbRepository<Course> {
}
