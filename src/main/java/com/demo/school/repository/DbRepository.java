package com.demo.school.repository;

import com.demo.school.entity.DbEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DbRepository<T extends DbEntity> extends JpaRepository<T, Long> {
}
