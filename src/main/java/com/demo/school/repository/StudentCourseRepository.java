package com.demo.school.repository;

import com.demo.school.entity.Course;
import com.demo.school.entity.Student;
import com.demo.school.entity.StudentCourse;
import com.demo.school.response.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface StudentCourseRepository extends DbRepository<StudentCourse> {


    @Query("select new Map(sc.student as student, sc.course as course) from StudentCourse sc where" +
            " (:studentId is null or sc.student.id =:studentId) and (:courseId is null or sc.course.id = :courseId)")
    Page<Map<String, Object>> report(@Param("studentId")Long studentId, @Param("courseId")Long courseId , Pageable pageable);
    Long countAllByCourse(Course course);
    Long countAllByStudent(Student student);
    Boolean existsByCourseAndStudent(Course course, Student student);

    @Query("select s from Student s where s not in (select sc.student from StudentCourse sc)")
    Page<Student> studentWithNoCourse(Pageable pageable);

    @Query("select c from Course c where c not in (select sc.course from StudentCourse sc)")
    Page<Course> courseWithNoStudent(Pageable pageable);
}
