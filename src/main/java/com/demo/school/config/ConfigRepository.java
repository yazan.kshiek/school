package com.demo.school.config;

import com.demo.school.entity.Config;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ConfigRepository extends JpaRepository<Config, Long> {

    Config getById(Long id);
}
