package com.demo.school.config;

import com.demo.school.entity.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigService {

    @Autowired ConfigRepository configRepository;

    public Config getConfig(){
        return configRepository.getById(Config.CONFIG_ID);
    }
}
