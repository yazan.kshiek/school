package com.demo.school.serializer;

import com.demo.school.entity.DbEntity;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class IdLabelSerializer extends JsonSerializer<DbEntity> {


    public void serialize(DbEntity e, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (e == null) {
            jsonGenerator.writeNull();
        } else {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", e.getId());
            jsonGenerator.writeEndObject();
        }
    }
}
