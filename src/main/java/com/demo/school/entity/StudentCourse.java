package com.demo.school.entity;

import com.demo.school.serializer.IdLabelSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STUDENT_COURSE", uniqueConstraints = {@UniqueConstraint(columnNames={"COURSE_ID","STUDENT_ID"})})
public class StudentCourse extends DbEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="COURSE_ID", referencedColumnName = "ID")
    @JsonSerialize(using = IdLabelSerializer.class)
    @NotNull
    private Course course;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="STUDENT_ID", referencedColumnName = "ID")
    @JsonSerialize(using = IdLabelSerializer.class)
    @NotNull
    private Student student;

    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof StudentCourse))
            return false;

        StudentCourse other = (StudentCourse) o;
        return this.getCourse() != null && this.getStudent() != null
                && other.getStudent() != null && other.getCourse() != null
                && this.getStudent().getId().equals(other.getStudent().getId())
                && this.getCourse().getId().equals(other.getCourse().getId());
    }
}