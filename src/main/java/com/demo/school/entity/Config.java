package com.demo.school.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CONFIG")
@EqualsAndHashCode(callSuper = true)
public class Config extends DbEntity{

    public static final Long CONFIG_ID = 1L;

    @Column(name = "MAX_STUDENTS_PER_COURSE")
    private Integer maxStudentsPerCourse;

    @Column(name = "MAX_COURSES_FOR_STUDENT")
    private Integer maxCoursesForStudent;
}
