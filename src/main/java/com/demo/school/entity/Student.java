package com.demo.school.entity;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STUDENTS")
public class Student extends DbEntity{

    @Column(name = "NAME")
    @NotBlank
    private String name;
}
