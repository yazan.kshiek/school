package com.demo.school.controller;

import com.demo.school.entity.Course;
import com.demo.school.entity.Student;
import com.demo.school.entity.StudentCourse;

import com.demo.school.exception.CustomException;
import com.demo.school.exception.RestApiResponse;
import com.demo.school.repository.DbRepository;

import com.demo.school.repository.StudentRepository;
import com.demo.school.response.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/students")
public class StudentController extends DbController<Student> {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    StudentCourseController studentCourseController;

    @Override
    public DbRepository<Student> getRepository() {
        return studentRepository;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/register/{course-id}")
    public RestApiResponse<Boolean> register(@PathVariable("id")Student student, @PathVariable("course-id")Course course)
            throws CustomException {

        studentCourseController.canRegistered(student, course);
        studentCourseController.save(StudentCourse.builder().course(course).student(student).build());
        return RestApiResponse.ok(true);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/report")
    public RestApiResponse<Report> report(@RequestParam(name = "studentId", required = false)Long studentId,
        @RequestParam(name = "courseId", required = false)Long courseId,
        @RequestParam(name = "studentWithNoCourse", required = false, defaultValue = "false")boolean studentWithNoCourse,
        @RequestParam(name = "courseWithNoStudent", required = false, defaultValue = "false")boolean courseWithNoStudent,
            Pageable pageable){

        Report r = new Report();

        if(courseWithNoStudent)
            r = studentCourseController.courseWithNoStudent(r, pageable);

        if(studentWithNoCourse)
            r = studentCourseController.studentWithNoCourse(r, pageable);

        //if(!studentWithNoCourse && !courseWithNoStudent)
            r = studentCourseController.report(r, studentId, courseId, pageable);

        return RestApiResponse.ok(r);
    }
}