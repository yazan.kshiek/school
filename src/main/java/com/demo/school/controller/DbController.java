package com.demo.school.controller;

import com.demo.school.entity.DbEntity;
import com.demo.school.exception.ApiMessageCode;
import com.demo.school.exception.CustomException;
import com.demo.school.exception.RestApiResponse;
import com.demo.school.repository.DbRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.Locale.ENGLISH;

@Controller
public abstract class DbController<T extends DbEntity>{

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;

    protected abstract DbRepository<T> getRepository();

    protected MessageSource getMessageSource() {
        return msgSrc;
    }

    @Operation(summary = "Get a entity by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the item", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/{id}"}, method = {RequestMethod.GET})
    public ResponseEntity<RestApiResponse<T>> getById(@PathVariable("id") Long id) throws CustomException {

        T entity = this.getRepository().findById(id)
                .orElseThrow(() -> CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND))
                        .status(HttpStatus.NOT_FOUND).build());

        return new ResponseEntity<>(RestApiResponse.ok(entity), HttpStatus.OK);
    }

    @Operation(summary = "Get page")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the item", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content)})
    @RequestMapping(value = {""}, method = {RequestMethod.GET})
    public ResponseEntity<RestApiResponse<Page<T>>> getPage(Pageable pageable){
        return new ResponseEntity<>(RestApiResponse.ok(this.getRepository().findAll(pageable)), HttpStatus.OK);
    }

    @Operation(summary = "Create a new entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Entity created", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content)})
    @RequestMapping(method = {RequestMethod.POST})
    @Transactional
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<RestApiResponse<T>> create(@RequestBody T entity) throws CustomException {
        entity = createEntity(entity);

        return new ResponseEntity<>(RestApiResponse.ok(entity), HttpStatus.CREATED);
    }

    @Operation(summary = "Create a list of new entities")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Entities created", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content)})
    @RequestMapping(value = {"/all"}, method = {RequestMethod.POST})
    @Transactional
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<RestApiResponse<List<T>>> create(@RequestBody List<T> entities) throws CustomException {

        List<T> createdList = new ArrayList<>();
        for (T entity : entities) {
            if (entity.getId() != null)
                continue;
            createdList.add(createEntity(entity));
        }
        return new ResponseEntity<>(RestApiResponse.ok(createdList), HttpStatus.CREATED);
    }

    protected T createEntity(@Valid T entity) throws CustomException {
        if (entity.getId() != null)
            throw CustomException.builder().msg(extractApiMsg(ApiMessageCode.INVALID_DATA)).status(HttpStatus.BAD_REQUEST).build();

        entity = this.getRepository().save(entity);
        return entity;
    }

    @Operation(summary = "Delete entity by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity deleted", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/{id}"}, method = {RequestMethod.DELETE})
    @Transactional
    public ResponseEntity<RestApiResponse<Boolean>> delete(@PathVariable("id") T entity) {

        return new ResponseEntity<>(RestApiResponse.ok(deleteEntity(entity)), HttpStatus.OK);
    }

    @Operation(summary = "Delete list of entities by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity deleted", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/all/{ids}"}, method = {RequestMethod.DELETE})
    @Transactional
    public ResponseEntity<RestApiResponse<Boolean>> delete(@PathVariable("ids") List<Long> ids) throws CustomException {

        for (Long id : ids) {
            deleteEntity(getRepository().findById(id)
                    .orElseThrow(() -> CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build()));
        }
        return new ResponseEntity<>(RestApiResponse.ok(true), HttpStatus.OK);
    }

    protected Boolean deleteEntity(T entity) {
        this.getRepository().delete(entity);
        return true;
    }

    @Operation(summary = "Update entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity updated", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(method = {RequestMethod.PUT})
    @Transactional
    public ResponseEntity<RestApiResponse<T>> update(@RequestBody T entity) throws IOException, CustomException {

        return new ResponseEntity<>(RestApiResponse.ok(updateEntity(entity)), HttpStatus.OK);
    }

    @Operation(summary = "Update entities")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity updated", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/all"}, method = {RequestMethod.PUT})
    @Transactional
    public ResponseEntity<RestApiResponse<List<T>>> update(@RequestBody List<T> list) throws IOException, CustomException {

        List<T> updated = new ArrayList<>();
        for (T entity : list) {
            updated.add(updateEntity(entity));
        }
        return new ResponseEntity<>(RestApiResponse.ok(updated), HttpStatus.OK);
    }

    protected T updateEntity(T entity) throws IOException, CustomException {
        @Valid T updated = mergeEntityWithJsonNode(getJsonNode(entity, true), getRepository());
        return getRepository().save(updated);
    }

    public <T extends DbEntity> T mergeEntityWithJsonNode(JsonNode node, DbRepository<T> repository) throws IOException, CustomException {
        if (node == null)
            return null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        if (!node.has("id") || node.get("id").isNull() || node.get("id").asText().isEmpty())
            throw CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build();

        T entity = repository.findById(node.get("id").asLong()).orElseThrow(
                () -> CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build());
        return objectMapper.readerForUpdating(entity).readValue(node);
    }

    public static JsonNode getJsonNode(Object o, boolean excludeNull) {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        if (excludeNull)
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper.convertValue(o, JsonNode.class);
    }

    public String extractApiMsg(ApiMessageCode code){
        return getMessageSource().getMessage(code.getMessage(), null, ENGLISH);
    }
}
