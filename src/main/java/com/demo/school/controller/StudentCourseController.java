package com.demo.school.controller;

import com.demo.school.config.ConfigService;
import com.demo.school.entity.Config;
import com.demo.school.entity.Course;
import com.demo.school.entity.Student;
import com.demo.school.entity.StudentCourse;
import com.demo.school.exception.ApiMessageCode;
import com.demo.school.exception.CustomException;
import com.demo.school.exception.RestApiResponse;
import com.demo.school.repository.DbRepository;
import com.demo.school.repository.StudentCourseRepository;
import com.demo.school.response.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Locale.ENGLISH;

@Controller
public class StudentCourseController{

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;

    @Autowired
    StudentCourseRepository studentCourseRepository;

    @Autowired
    ConfigService configService;

    public boolean canRegistered(Student student, Course course) throws CustomException {
        if(course == null || student == null)
            throw CustomException
                    .builder()
                    .msg(msgSrc.getMessage(ApiMessageCode.NOT_FOUND.getMessage(), null, ENGLISH))
                    .status(HttpStatus.NOT_FOUND).build();

        if(studentCourseRepository.existsByCourseAndStudent(course, student))
            throw CustomException
                    .builder()
                    .msg(msgSrc.getMessage(ApiMessageCode.STUDENT_REGISTERED.getMessage(), null, ENGLISH))
                    .status(HttpStatus.BAD_REQUEST).build();

        Config config = configService.getConfig();
        Long studentCourses = studentCourseRepository.countAllByStudent(student);
        if(studentCourses >= config.getMaxCoursesForStudent())
            throw CustomException
                    .builder()
                    .msg(msgSrc.getMessage(ApiMessageCode.MAX_COURSE_EXCEEDED.getMessage(), null, ENGLISH))
                    .status(HttpStatus.BAD_REQUEST).build();

        Long courseStudents = studentCourseRepository.countAllByCourse(course);
        if(courseStudents >= config.getMaxStudentsPerCourse())
            throw CustomException
                    .builder()
                    .msg(msgSrc.getMessage(ApiMessageCode.MAX_STUDENT_EXCEEDED.getMessage(), null, ENGLISH))
                    .status(HttpStatus.BAD_REQUEST).build();
        return true;
    }

    @Transactional
    public StudentCourse save(StudentCourse studentCourse){
        return studentCourseRepository.save(studentCourse);
    }

    public Report report(Report report, Long studentId, Long courseId, Pageable pageable){
        Page<Map<String, Object>> data = studentCourseRepository.report(studentId, courseId, pageable);


        for (Map map: data.getContent()) {
            Student s = (Student) map.get("student");
            Course c = (Course) map.get("course");



            if(s != null) {
                String studentKey = s.getId() + "_" + s.getName();
                if (report.getCoursePerStudent() == null)
                    report.setCoursePerStudent(new HashMap<>());
                if (!report.getCoursePerStudent().containsKey(studentKey))
                    report.getCoursePerStudent().put(studentKey, new ArrayList<>());


                report.getCoursePerStudent().get(studentKey).add(c);
            }
            if(c != null) {
                String courseKey = c.getId()+ "_" + c.getName();
                if (report.getStudentsPerCourse() == null)
                    report.setStudentsPerCourse(new HashMap<>());
                if (!report.getStudentsPerCourse().containsKey(courseKey))
                    report.getStudentsPerCourse().put(courseKey, new ArrayList<>());
                report.getStudentsPerCourse().get(courseKey).add(s);
            }
        }
        return report;
    }

    public Report studentWithNoCourse(Report report, Pageable pageable) {
        Page<Student> page = studentCourseRepository.studentWithNoCourse(pageable);
        report.setStudentsWithoutCourse(page.getContent());
        return report;
    }

    public Report courseWithNoStudent(Report report, Pageable pageable) {
        Page<Course> page = studentCourseRepository.courseWithNoStudent(pageable);
        report.setCoursesWithoutStudent(page.getContent());
        return report;
    }
}
