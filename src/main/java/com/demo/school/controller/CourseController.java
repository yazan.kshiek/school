package com.demo.school.controller;

import com.demo.school.entity.Course;
import com.demo.school.repository.CourseRepository;
import com.demo.school.repository.DbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/courses")
public class CourseController extends DbController<Course> {

    @Autowired
    CourseRepository courseRepository;

    @Override
    public DbRepository<Course> getRepository() {
        return courseRepository;
    }
}
