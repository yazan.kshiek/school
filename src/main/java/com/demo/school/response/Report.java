package com.demo.school.response;

import com.demo.school.entity.Course;
import com.demo.school.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Report implements Serializable {

    Map<String, List<Course>> coursePerStudent;
    Map<String, List<Student>> studentsPerCourse;
    List<Course> coursesWithoutStudent;
    List<Student> studentsWithoutCourse;
}
