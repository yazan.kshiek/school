package com.demo.school.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ApiMessageCode {

    GENERAL_EXCEPTION("API.SERVER.ERROR"),
    INVALID_FIELD("API.INVALID.FIELD"),
    INVALID_DATA("API.INVALID.DATA"),
    FIELD_MANDATORY("API.MISSING.DATA"),
    GENERAL_MSG("API.GENERAL_MSG"),
    MAX_COURSE_EXCEEDED("API.MAX_COURSE_EXCEEDED"),
    MAX_STUDENT_EXCEEDED("API.MAX_STUDENT_EXCEEDED"),
    NOT_FOUND("API.NOT_FOUND"), STUDENT_REGISTERED("API.STUDENT_REGISTERED");

    private String message;
}
