package com.demo.school.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Locale.ENGLISH;


@ControllerAdvice(annotations = RestController.class)
@Slf4j
@Configuration
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<RestApiResponse<String>> handleGenericException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(
                RestApiResponse.builder()
                        .result(msgSrc.getMessage(ApiMessageCode.GENERAL_MSG.getMessage(), null, ENGLISH))
                        .status(false)
                        .build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = CustomException.class)
    public ResponseEntity<RestApiResponse<String>> handleCustomException(CustomException ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(new RestApiResponse<>(false, ex.getMsg()), ex.getStatus());
    }

    @ExceptionHandler(value = {SQLIntegrityConstraintViolationException.class})
    public ResponseEntity<RestApiResponse<String>> handleSQLIntegrityConstraintViolationException(
            SQLIntegrityConstraintViolationException ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(new RestApiResponse<>(false, ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(ex.getMessage(), ex);

        List<String> errorMsg = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .map(m->
                        msgSrc.getMessage(
                                m.replace("{", "").replace("}", ""),null, ENGLISH))
                .collect(Collectors.toList());

        if(errorMsg.isEmpty())
            errorMsg.add(ex.getMessage());
        return new ResponseEntity<>(new RestApiResponse<>(false, errorMsg), HttpStatus.BAD_REQUEST);
    }
}
