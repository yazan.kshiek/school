package com.demo.school.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class RestApiResponse<T>{

	private boolean status;
	private T result;

	public static <T> RestApiResponse<T> ok(T result) {
		return RestApiResponse.<T>builder()
							  .status(true)
							  .result(result)
							  .build();
	}
}
